import { Module } from '@nestjs/common';
import { ExpenseController } from './expense/expense.controller';
import { ExpenseService } from './expense/expense.service';
import { ExpenseModule } from './expense/expense.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Expense } from './expense/entities/expense.entity';
import Config, {
  DEV_MODE,
  MYSQL_DATABASE,
  MYSQL_HOST,
} from './configuration/config';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [Config],
    }),
    ExpenseModule,
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: MYSQL_HOST,
      port: 3306,
      username: 'root',
      password: 'root',
      charset: 'UTF8_GENERAL_CI',
      database: MYSQL_DATABASE,
      entities: [Expense],
      dropSchema: DEV_MODE,
      synchronize: DEV_MODE,
      debug: DEV_MODE,
    }),
  ],
  controllers: [ExpenseController],
  providers: [ExpenseService],
})
export class AppModule {}
