import * as process from 'process';

const PORT: number = 4000;

export const MYSQL_HOST: string = process.env.MYSQL_HOST
  ? process.env.MYSQL_HOST
  : 'localhost';

export const MYSQL_DATABASE = process.env.MYSQL_DB
  ? process.env.MYSQL_DB
  : 'expense-tracker';

export const DEV_MODE: boolean = process.env.DEV_MODE
  ? Boolean(process.env.DEV_MODE)
  : false;

export default () => ({
  port: process.env.PORT ? parseInt(process.env.PORT) : PORT,
});
