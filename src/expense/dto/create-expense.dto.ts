import { Category } from '../entities/category.enum';

export class CreateExpenseDto {
  public title: string;

  public amount: number;

  public date: Date;

  public category: Category;
}
