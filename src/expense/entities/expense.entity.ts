import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { Category } from './category.enum';

@Entity()
export class Expense {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column()
  public title: string;

  @Column({ type: 'float' })
  public amount: number;

  @Column()
  public date: Date;

  @Column()
  public category: Category;
}
