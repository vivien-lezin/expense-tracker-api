import { Injectable } from '@nestjs/common';
import { CreateExpenseDto } from './dto/create-expense.dto';
import { UpdateExpenseDto } from './dto/update-expense.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Expense } from './entities/expense.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ExpenseService {
  constructor(
    @InjectRepository(Expense)
    private readonly _expenseRepo: Repository<Expense>,
  ) {}

  create(createExpenseDto: CreateExpenseDto) {
    return this._expenseRepo.save(createExpenseDto);
  }

  findAll() {
    return this._expenseRepo.find();
  }

  findOne(id: number) {
    return this._expenseRepo.findOneBy({ id: id });
  }

  update(id: number, updateExpenseDto: UpdateExpenseDto) {
    return this._expenseRepo.update({ id: id }, updateExpenseDto);
  }

  remove(id: number) {
    return this._expenseRepo.delete({ id: id });
  }
}
