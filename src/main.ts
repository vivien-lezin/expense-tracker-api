import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import Config from './configuration/config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });
  await app.listen(Config().port);
}
bootstrap();
